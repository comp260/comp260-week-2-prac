# README #

The first prac exercise for COMP260 students. 

### Task ###

1. Fork a copy of this repository into your account.
2. Check it out to your machine.
3. Edit the project
4. Push the changes back to your account.
5. Create a pull-request to submit your changes.

See the prac sheet for details